<%-- 
    Document   : definicion
    Created on : May 8, 2021, 12:17:28 AM
    Author     : mlara
--%>
<%@page import="com.controller.entity.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabras definicion = (Palabras) request.getAttribute("definicion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        <table border="1">
       
        <tbody>
            <tr>
                <td><h1>Palabra</h1></td>
                <td><h2><%= definicion.getPalabra().toString() %></h2></td>
            </tr>
            <tr>
                <td><h1>Significado</h1></td>
                <td><h2><%= definicion.getDefinicion().toString() %></h2> </td>
            </tr>
            <tr>
                <td><h2>Fuente:</h2></td>
                <td><h2>Diccionario Oxford</h2></td>
            </tr>
        </tbody>
    </table>
        
        <form  name="form" action="ConsultaController" method="POST">
           
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver a Consultar</button>
         </form>
    </body>
</html><table border="1">
  
   

</table>

