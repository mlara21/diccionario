<%-- 
    Document   : index
    Created on : May 8, 2021, 3:24:44 PM
    Author     : mlara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario Oxford</title>
    </head>
    <body>
            <h1>Diccionario Español - Oxford</h1>
        <form  name="form" action="ConsultaController" method="POST">
            Ingrese Palabra a Consultar:
                <input type="text" id="idbuscar" name="idbuscar">
                
            <br> <br>

           <button type="submit" name="accion" value="buscar" class="btn btn-success">Buscar Palabra</button>
            <br><br>
           <button type="submit" name="accion" value="listar" class="btn btn-success">Consultar Busquedas Anteriores</button>
        </form>
            <p>Miguel Lara - Sección 50</p>
            <p>URL Bitbucket: https://mlara21@bitbucket.org/mlara21/diccionario.git</p>
            <p>Aplicación en Heroku: https://diccionariofinal.herokuapp.com/</p>
    </body>
</html>
