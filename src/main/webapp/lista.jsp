<%-- 
    Document   : historial
    Created on : May 7, 2021, 7:16:19 PM
    Author     : mlara
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.controller.entity.Palabras"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabras> palabras = (List<Palabras>) request.getAttribute("listaPalabras");
    Iterator<Palabras> itPalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
    </head>
    <body>
        <h1>Consultas Busquedas Anteriores</h1>
         <form  name="form" action="ConsultaController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>ID Palabra</th>
                    <th>Palabra</th>
                    <th>Significado</th>
                    <th>Fecha</th>
         
                    </thead>
                    <tbody>
                        <%while (itPalabras.hasNext()) {
                       Palabras cm = itPalabras.next();%>
                        <tr>
                            <td id="id_palabra"><%= cm.getIdPalabra()%></td>
                            <td id="palabra"><%= cm.getPalabra()%></td>
                            <td id="definicion"><%= cm.getDefinicion()%></td>
                            <td id="fecha"><%= cm.getFecha()%></td>
                         
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
     
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver a Consultar</button>
        
         </form>   
    </body>
